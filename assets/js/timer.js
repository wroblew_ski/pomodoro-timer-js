var pomodoro = {
  started: false,
  fillerHeight: 0,
  fillerIncrement: 0,
  interval: null,
  minutesDom: null,
  secondsDom: null,
  fillerDom: null,
  mins: null,
  secs: null,
  app: document.body,
  modal: document.getElementById('breakModal'),
  title: document.getElementById('title'),
  modalEndBreak: document.getElementById('endbreakModal'),
  timer: document.getElementById("timer"),
  timerInputs: document.getElementById("timer-inputs"),
  alert: document.getElementById('alert'),

  init: function() {
    var self = this;
    var clicked = false;
    this.minutesDom = document.querySelector('#minutes');
    this.secondsDom = document.querySelector('#seconds');
    this.fillerDom = document.querySelector('#filler');
    this.interval = setInterval(function() {
      self.intervalCallback.apply(self);
    }, 1000);
    document.querySelector('#work').onclick = function() {
      if (this.getAttribute("data-status") == "working") {
        self.resetTimer();
        self.startWork.apply(self);
      } else {
        this.setAttribute("data-status", "working");
        self.startWork.apply(self);
        this.innerHTML = "Reset"
      }
    };
    document.querySelector('#startBreak').onclick = function() {
      self.startBreak.apply(self);
    };
    document.querySelector('#stop').onclick = function() {
      self.updateDom();
      if (clicked) {
        clicked = false;
        self.resumeTimer.apply(self);
        this.innerHTML = "Pause";
      } else {
        clicked = true;
        self.pauseTimer.apply(self);
        this.innerHTML = "Resume";
      }
    };
    document.querySelector('#endBreak').onclick = function() {
      self.modalEndBreak.classList.remove("open");
      self.startWork();
    };
  },

  resetVariables: function(mins, secs, started) {
    var self = this;
    mins = document.getElementById('input-minutes');
    secs = document.getElementById('input-seconds');
    self.mins = mins.value;
    self.secs = secs.value;

    function validate() {
      if (self.secs < 0 || self.secs.length == 0 || self.mins.length == 0 || self.mins < 0) {
        secs.value = Math.max(0, self.secs);
        mins.value = Math.max(0, self.mins);
        self.started = false;
      } else if (self.secs > 60 && self.mins.length != 0 && self.mins >= 0) {
        function convert() {
          var a = parseInt(self.mins);
          var b = Math.floor(self.secs / 60);
          var c = Math.floor(self.secs % 3600 % 60);
          self.mins = parseInt(a, 10) + parseInt(b, 10);
          self.secs = c;
        }
        convert()
        mins.value = self.mins;
        secs.value = self.secs;
      }
    }
    totalTime = Number(self.mins * 60) + Number(self.secs);
    this.started = started;
    this.fillerIncrement = self.app.scrollHeight / totalTime;
    this.fillerHeight = 0;
    validate();
  },
  breakVariables: function(mins, secs, started) {
    var self = this;
    mins = document.getElementById('input-break');
    secs = 0;
    self.mins = mins.value;
    self.secs = secs;
    totalTime = Number(self.mins * 60) + Number(self.secs);
    this.started = started;
    this.fillerIncrement = self.app.scrollHeight / totalTime;
    this.fillerHeight = 0;
  },
  startWork: function() {
    this.resetVariables(this.mins, this.secs, true);
    this.timer.setAttribute("data-status", "workTime");
  },
  startBreak: function() {
    this.breakVariables(this.mins, 0, true);
    this.modal.classList.remove("open");
    this.timer.setAttribute("data-status", "break");
  },
  pauseTimer: function() {
    clearInterval(this.interval);
  },
  resumeTimer: function() {
    var self = this;
    this.interval = setInterval(function() {
      self.intervalCallback.apply(self);
    }, 1000);
  },

  toDoubleDigit: function(num) {
    if (num < 10) {
      return "0" + parseInt(num, 10);
    }
    return num;
  },
  updateDom: function() {
    this.minutesDom.innerHTML = this.toDoubleDigit(this.mins);
    this.secondsDom.innerHTML = this.toDoubleDigit(this.secs);
    this.fillerHeight = this.fillerHeight + this.fillerIncrement;
    this.fillerDom.style.height = this.fillerHeight + 'px';
  },
  resetTimer: function() {
    mins = document.getElementById('input-minutes');
    secs = document.getElementById('input-seconds');
    this.minutesDom.innerHTML = mins.value;
    this.secondsDom.innerHTML = secs.value;
  },
  intervalCallback: function() {
    var self = this;
    if (!this.started) return false;
    if (this.secs == 0) {
      if (this.mins == 0) {
        this.timerComplete();
        if (self.timer.getAttribute("data-status") == "workTime") {
          self.title.innerHTML = "BREAK TIME";
          responsiveVoice.speak("Przerwa!", "Polish Female", {});
          this.modal.classList.add("open");
          this.timerInputs.style.display = 'none';
          document.querySelector('#work').disabled = true;
        } else {
          self.title.innerHTML = "WORK TIME";
          this.modalEndBreak.classList.add("open");
          responsiveVoice.speak("Koniec przerwy!", "Polish Female", {});
          this.timerInputs.style.display = 'flex';
          document.querySelector('#work').disabled = false;
        }
        return;
      }
      this.secs = 59;
      this.mins--;
    } else {
      this.secs--;
    }
    this.updateDom();
  },
  timerComplete: function() {
    this.started = false;
    this.fillerHeight = 0;
  }
};

window.onload = function() {
  pomodoro.init();
};
